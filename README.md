# Code repositories API / common

This package offers a wrappers to GitLab/GitHub/BitBucket APIs.

The motivation for this package was to access hosted *repositories* and their *issues*, for them to be used in a project/time tracking app.
Hence I wanted to have a common API to access those objects from these three major code hosting providers.

## How it works?

This *common* package **isn't standalone**. By this I mean that it needs a companion package in order to work. The *common* package performs calls to APIs through GET or POST calls (so far). Because the tool is meant to be usable both from Node.js or from the browser, the actual requesting is left to the companion package, which we could call the *request strategy* package.

Currently there is one working companion package: code-repositories-api-node. Another is planned for use from the browser within Angular apps (code-repositories-api-angular).


## TODO

- Document
- Test (underway, in separate package)
- When setting token, also set scopes, so we can prevent using some methods if they are forbidden by scope.