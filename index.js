// your library here
(function (root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define('code-repositories-api-common', [], factory);
  } else if (typeof module !== 'undefined' && typeof module.exports === 'object') {
    // CommonJS support (for us webpack/browserify/ComponentJS folks)
    module.exports = factory();
  } else {
    // in the case of no module loading system
    // then don't worry about creating a global
    // variable like you would in normal UMD.
    // It's not really helpful... Just call your factory
    return factory();
  }
}(this, function () {
  'use strict';

  // function passLog(val) {
  //   console.log('\n## passLog', val); return val;
  // }

  /**
   * Encode provider credentials to base64
   */
  function getEncodedCredentials(credentials) {
    const credentialsString = credentials.clientId + ':' + credentials.secret;
    return new Buffer(credentialsString).toString('base64');
  }

  /*-----------------------*
   | Bitbucket Strategy
   *-----------------------*
   |
   */
  var BitbucketStrategy = (function() {

    /**
     * Strategy for BitBucket
     * @class
     * @classdesc Strategy for BitBucket
     */

    /**
     * Strategy for BitBucket
     * @constructor
     */
    function Bitbucket(requestStrategy, credentials) {
      this.requestStrategy = requestStrategy;
      this.baseUri = 'https://api.bitbucket.org/2.0';
      this.tokenUri = 'https://bitbucket.org/site/oauth2/access_token';
      this.username = null;
      this.accessToken = null;
      this.accessTokenExpiresAt = null;
      this.refreshToken = null;
      this.scopes = null;
      this.basicAuthString = 'Basic ' + getEncodedCredentials(credentials);
    }

    /**
     * Set Authorization header with base64-encoded credentials
     * (BitBucket token requests must be authenticated this way)
     */
    Bitbucket.prototype.enableBasicAuth = function() {
      if(this.basicAuthString === null) {
        throw new Error('Basic Auth credentials should have been set by constructor');
      }
      this.headers = {
        Authorization: this.basicAuthString
      };
    }

    /**
     * Request an access token
     * @param {string} code - Authorization code sent back to callback URL
     * @returns {Promise}
     */
    Bitbucket.prototype.requestAccessToken = function(code) {
      if(typeof code !== 'string') {
        throw new Error('requestAccessToken must be called with "code" argument (string)');
      }
      this.enableBasicAuth();
      return this.requestStrategy.post({
        grant_type: 'authorization_code',
        code
      }, { url: this.tokenUri, headers: this.headers })
      .then(response => {
        return {
          accessToken: response.access_token,
          refreshToken: response.refresh_token,
          expiresAt: (new Date()).getTime() + response.expires_in,
          scopes: response.scopes
        };
      })
    }

    /**
     * Check if access token must be refreshed
     * @returns {boolean}
     */
    Bitbucket.prototype.mustRefreshAccessToken = function() {
      return (new Date()).getTime() >= this.accessTokenExpiresAt;
    }

    /**
     * Refresh access token
     * @returns {Promise}
     */
    Bitbucket.prototype.refreshAccessToken = function() {
      const self = this;
      if(this.refreshToken === null) {
        throw new Error('Refresh token should have been set before');
      }
      this.enableBasicAuth();
      return this.requestStrategy.post({
        grant_type: 'refresh_token',
        refresh_token: this.refreshToken
      }, { url: this.tokenUri, headers: this.headers })
      .then(response => {
        const data = {
          accessToken: response.access_token,
          refreshToken: response.refresh_token,
          expiresAt: (new Date()).getTime() + response.expires_in,
          scopes: response.scopes
        };

        self.setTokenData.call(self, data);
        return {
          didRefreshToken: true
        };
      });
    }

    /**
     * Set token data, either with data returned from API call, or from DB
     * @param {object} data - hash containing accessToken, refreshToken, expiresAt, scopes
     */
    Bitbucket.prototype.setTokenData = function(data) {
      this.accessToken = data.accessToken;
      this.refreshToken = data.refreshToken;
      this.accessTokenExpiresAt = data.expiresAt;
      this.scopes = data.scopes.split('');
      if(data.username) {
        this.username = data.username;
      }

      this.headers = {
        Authorization: 'Bearer ' + this.accessToken
      };
      this.requestStrategy.setHeaders(this.headers);
    }

    /**
     * Set tokens and refresh access tokens if needed
     */
    Bitbucket.prototype.setTokens = function(data) {
      console.log('\n### Bitbucket.setTokens', data)
      this.setTokenData(data);
      return this.mustRefreshAccessToken() ?
        this.refreshAccessToken() : Promise.resolve({
          didRefreshToken: false
        });
    }

    // Bitbucket.prototype.setToken = function(token) {
    //   var self = this;
    //   this.token = token;
    //   this.headers = {
    //     Authorization: 'Bearer ' + this.token
    //   };
    //   this.setPath('/user')
    //   // console.log('Bitbucket query logged-in user');
    //   return this.requestStrategy.get()
    //   .then(({ body }) => {
    //     console.log('GET /user response', body);
    //     self.username = body.username;
    //     return body;
    //   })
    //   .catch(err => {
    //     console.error('GET /user failed', err);
    //     throw err;
    //   })
    // }

    Bitbucket.prototype.setPath = function(relativePath) {
      if(this.accessToken === undefined) {
        throw new Error('token undefined in Bitbucket strategy');
      }
      this.requestStrategy.setup(this.baseUri, relativePath, {
        Authorization: 'Bearer ' + this.accessToken
      })
    };

    Bitbucket.prototype.get = function(url, headers) {
      return this.requestStrategy.get({ url, headers });
    };

    Bitbucket.prototype.getUser = function() {
      var self = this;
      this.setPath('/user');
      return this.get()
      .then(({ body }) => {
        self.username = body.username;
        return body;
      })
    };

    Bitbucket.prototype.getProjects = function() {
      this.setPath('/repositories/' + this.username);
      return this.promiseLoop({
        next: this.baseUri + '/repositories/' + this.username,
        values: []
      })
      .then(({ values }) => (
        values.map(record => ({
          uuid: record.uuid,
          name: record.name,
          fullName: record.full_name,
          htmlUrl: record.links.html.href
        }))
      ));
    };


    Bitbucket.prototype.getCommitsFor = function(repoName) {
      this.setPath('/repositories/' + this.username + '/' + repoName + '/commits');
      return this.get()
      .then(({ body }) => { 
        return body.values.map(commit => {
        return { sha: commit.hash, message: commit.message };
      }); });
    };

    Bitbucket.prototype.mapState = function(origState) {
      const map = {
        'new': 'open',
        open: 'open',
        resolved: 'closed',
        closed: 'closed'
      }
      return map[origState];
    }

    Bitbucket.prototype.getIssuesFor = function(repo) {
      const { name } = repo;
      return this.promiseLoop({
        next: this.baseUri + '/repositories/' + this.username + '/' + name + '/issues',
        values: []
      })
      .then(({ values }) => (values.map(issue => ({
        iid: issue.id,
        title: issue.title,
        description: issue.content.html,
        url: issue.links.html.href,
        createdAt: issue.created_on,
        updatedAt: issue.updated_on,
        state: this.mapState(issue.state),
        labels: []
      }))));
    };

    Bitbucket.prototype.createIssue = function(repo, attributes) {
      const { name } = repo;
      this.setPath('/repositories/' + this.username + '/' + name + '/issues');
      return this.requestStrategy.post(attributes);
    }

    Bitbucket.prototype.promiseLoop = function(res) {
      var that = this;
        if(res.next) {
          return this.requestStrategy.get({ url: res.next, headers: this.requestStrategy.headers })
          .then(({ body }) => that.promiseLoop.call(that, { next: body.next, values: res.values.concat(body.values) }));
        }
        else {
          return Promise.resolve(res);
        }
    }

    return Bitbucket;

  })();


  /*-----------------------*
   | GitHub Strategy
   *-----------------------*
   |
   */
  var GithubStrategy = (function() {

    function Github(requestStrategy, credentials) {
      this.requestStrategy = requestStrategy;
      this.baseUri = 'https://api.github.com';
      this.tokenUri = 'https://github.com/login/oauth/access_token';
      this.username = null;
      this.accessToken = null;
      this.scopes = null;
      this.headers = {};
      this.credentials = credentials;
    }


    /**
     * Request an access token
     * @param {string} code - Authorization code sent back to callback URL
     * @returns {Promise}
     */
    Github.prototype.requestAccessToken = function(code) {
      if(typeof code !== 'string') {
        throw new Error('requestAccessToken must be called with "code" argument (string)');
      }
      return this.requestStrategy.post(
        {
          client_id: this.credentials.clientId,
          client_secret: this.credentials.secret,
          code,
          accept: 'json'
        }, {
          url: this.tokenUri,
          headers: {}
        }
      )
      .then(response => {
        return {
          accessToken: response.access_token,
          scopes: response.scope
        };
      })
    }

    /**
     * Set token data, either with data returned from API call, or from DB
     * @param {object} data - hash containing accessToken, refreshToken, expiresAt, scopes
     */
    Github.prototype.setTokenData = function(data) {
      this.accessToken = data.accessToken;
      this.scopes = data.scopes.split('');
      if(data.username) {
        this.username = data.username;
      }

      this.headers = {
        Authorization: 'Bearer ' + this.accessToken
      };
      this.requestStrategy.setHeaders(this.headers);
    }

    /**
     * Set tokens and refresh access tokens if needed
     */
    Github.prototype.setTokens = function(data) {
      this.setTokenData(data);
      return Promise.resolve({
        didRefreshToken: false
      });
    }

    Github.prototype.getUser = function() {
      var self = this;
      this.setPath('/user');
      return this.get()
      .then(({ body }) => {
        self.username = body.login;
        return { username: body.login };
      })
    };


    // Github.prototype.setToken = function(token) {
    //   var self = this;
    //   this.token = token;
    //   this.headers = {
    //     Authorization: 'Bearer ' + this.token,
    //     'User-Agent': 'bhubr.apiwrapper.test.server'
    //   };
    //   this.setPath('/user')
    //   console.log('Github query logged-in user');
    //   return this.requestStrategy.get()
    //   .then(({ body }) => {
    //     console.log('GET /user response', body);
    //     self.username = body.login;
    //     return { username: body.login };
    //   })
    //   .catch(err => {
    //     console.error('GET /user failed', err);
    //     throw err;
    //   })
    // }

    Github.prototype.setPath = function(relativePath) {
      if(this.accessToken === undefined) {
        throw new Error('token undefined in Github strategy');
      }
      this.requestStrategy.setup(this.baseUri, relativePath, {
        Authorization: 'Bearer ' + this.accessToken,
        'User-Agent': 'bhubr.titang.server'
      })
    };

    Github.prototype.get = function(relativePath) {
      return this.requestStrategy.get();
    };

    Github.prototype.getProjects = function() {
      this.setPath('/user/repos');
      return this.requestStrategy.get()
      .then(({ body }) => (
        body.map(record => ({
          uuid: '' + record.id,
          name: record.name,
          fullName: record.full_name,
          htmlUrl: record.html_url
        }))
      ))
      .catch(err => {
        console.log('## err', err);
        throw err;
      });
    };

    Github.prototype.getCommitsFor = function(repoName) {
      this.setPath('/repos/' + this.username + '/' + repoName + '/commits');
      return this.get()
      .then(({ body }) => { return body.map(commit => {
        return { sha: commit.sha, message: commit.commit.message };
      }); });
    };

    Github.prototype.getIssuesFor = function(repo) {
      const { name } = repo;
      this.setPath('/repos/' + this.username + '/' + name + '/issues?state=all');
      return this.get()
      .then(({ body }) => (body.map(issue => ({
        iid: issue.number,
        title: issue.title,
        description: issue.body,
        url: issue.html_url,
        createdAt: issue.created_at,
        updatedAt: issue.updated_at,
        state: issue.state,
        labels: []
      }))));
    };

    return Github;

  })();


  /*-----------------------*
   | GitLab Strategy
   *-----------------------*
   |
   */
  var GitlabStrategy = (function() {

    function Gitlab(requestStrategy, credentials) {
      this.requestStrategy = requestStrategy;
      this.baseUri = 'https://gitlab.com/api/v4';
      this.tokenUri = 'https://gitlab.com/oauth/token';
      this.username = null;
      this.accessToken = null;
      this.scopes = null;
      this.headers = {};
      this.credentials = credentials;
    }


    /**
     * Request an access token
     * @param {string} code - Authorization code sent back to callback URL
     * @returns {Promise}
     */
    Gitlab.prototype.requestAccessToken = function(code) {
      if(typeof code !== 'string') {
        throw new Error('requestAccessToken must be called with "code" argument (string)');
      }
      return this.requestStrategy.post(
        {
          client_id: this.credentials.clientId,
          client_secret: this.credentials.secret,
          redirect_uri: this.credentials.redirectUri,
          grant_type: 'authorization_code',
          code
        }, {
          url: this.tokenUri,
          headers: {}
        }
      )
      .then(response => {
        return {
          accessToken: response.access_token,
          scopes: response.scope
        };
      })
    }

    /**
     * Set token data, either with data returned from API call, or from DB
     * @param {object} data - hash containing accessToken, refreshToken, expiresAt, scopes
     */
    Gitlab.prototype.setTokenData = function(data) {
      this.accessToken = data.accessToken;
      this.scopes = data.scopes.split('');
      if(data.username) {
        this.username = data.username;
      }

      this.headers = {
        Authorization: 'Bearer ' + this.accessToken
      };
      this.requestStrategy.setHeaders(this.headers);
    }

    /**
     * Set tokens and refresh access tokens if needed
     */
    Gitlab.prototype.setTokens = function(data) {
      this.setTokenData(data);
      return Promise.resolve({
        didRefreshToken: false
      });
    }

    Gitlab.prototype.getUser = function() {
      var self = this;
      this.setPath('/user');
      return this.get()
      .then(({ body }) => {
        self.username = body.username;
        return body;
      })
    };


    // Gitlab.prototype.setToken = function(token) {
    //   var self = this;
    //   this.token = token;
    //   this.headers = {
    //     Authorization: 'Bearer ' + this.token
    //   };
    //   this.setPath('/user')
    //   console.log('Gitlab query logged-in user');
    //   return this.requestStrategy.get()
    //   .then(({ body }) => {
    //     console.log('GET /user response', body);
    //     self.username = body.username;
    //     return body;
    //   })
    //   .catch(err => {
    //     console.error('GET /user failed', err);
    //     throw err;
    //   })
    // }

    Gitlab.prototype.setPath = function(relativePath) {
      if(this.accessToken === undefined) {
        throw new Error('token undefined in Gitlab strategy');
      }
      this.requestStrategy.setup(this.baseUri, relativePath, {
        Authorization: 'Bearer ' + this.accessToken
      })
    };
    Gitlab.prototype.get = function(relativePath) {
      return this.requestStrategy.get();
    };

    Gitlab.prototype.getProjects = function() {
      this.setPath('/projects?membership=yes');
      return this.requestStrategy.get()
      .then(({ body }) => (
        body.map(record => ({
          uuid: '' + record.id,
          name: record.name,
          fullName: record.name_with_namespace.replace(/ /g,''),
          htmlUrl: record.web_url
        }))
      ));
    };

    Gitlab.prototype.getCommitsFor = function(repoName) {
      this.setPath('/repos/' + this.username + '/' + repoName + '/commits');
      return this.get()
      .then(({ body }) => { return body.map(commit => {
        return { sha: commit.sha, message: commit.commit.message };
      }); });
    };

    Gitlab.prototype.mapState = function(origState) {
      const map = {
        opened: 'open',
        closed: 'closed'
      }
      return map[origState];
    }


    Gitlab.prototype.getIssuesFor = function(repo) {
      const { fullName } = repo;
      const encodedFullName = encodeURIComponent(fullName);
      return this.promiseLoop({
        linkHeaders: {
          next: this.baseUri + '/projects/' + encodedFullName + '/issues',  
        },
        isDone: false,
        iteration: 0,
        headers: {},
        body: []
      })
      .then(({ body }) => (body.map(issue => ({
        iid: issue.iid,
        title: issue.title,
        description: issue.description,
        url: issue.web_url,
        createdAt: issue.created_at,
        updatedAt: issue.updated_at,
        state: this.mapState(issue.state),
        labels: issue.labels
      }))));
    };

    Gitlab.prototype.createLabel = function(repo, attributes) {
      const { fullName } = repo;
      const encodedFullName = encodeURIComponent(fullName);
      this.setPath('/projects/' + encodedFullName + '/labels');
      return this.requestStrategy.post(attributes);
    }

    Gitlab.prototype.createIssue = function(repo, attributes) {
      const { fullName } = repo;
      const encodedFullName = encodeURIComponent(fullName);
      this.setPath('/projects/' + encodedFullName + '/issues');
      return this.requestStrategy.post(attributes);
    }

    Gitlab.prototype.updateLabel = function(repo, labelName, attributes) {
      const { name, color } = attributes;
      if(typeof labelName !== 'string') {
        throw new Error('updateLabel: 2nd argument (labelNale) should be current label name');
      }
      if(typeof name !== 'string' && typeof color !== 'string') {
        throw new Error('updateLabel: 3nd argument (attributes) should have at least name or color (strings)');
      }
      const { fullName } = repo;
      const encodedFullName = encodeURIComponent(fullName);
      const payload = { new_name: name, color };
      this.setPath('/projects/' + encodedFullName + '/labels/?name=' + labelName);
      return this.requestStrategy.put(payload);
    }

    Gitlab.prototype.updateIssue = function(repo, iid, payload) {
      console.log('## updateIssue payload', payload);
      const { fullName } = repo;
      const encodedFullName = encodeURIComponent(fullName);
      this.setPath('/projects/' + encodedFullName + '/issues/' + iid);
      return this.requestStrategy.put(payload);
    }

    Gitlab.prototype.deleteLabel = function(repo, labelName) {
      if(typeof labelName !== 'string') {
        throw new Error('updateLabel: 2nd argument (labelNale) should be current label name');
      }
      const { fullName } = repo;
      const encodedFullName = encodeURIComponent(fullName);
      this.setPath('/projects/' + encodedFullName + '/labels/?name=' + labelName);
      return this.requestStrategy.delete();
    }

    Gitlab.prototype.getLabels = function(repo) {
      const { fullName } = repo;
      const encodedFullName = encodeURIComponent(fullName);
      return this.promiseLoop({
        linkHeaders: {
          next: this.baseUri + '/projects/' + encodedFullName + '/labels',
        },
        isDone: false,
        iteration: 0,
        headers: {},
        body: []
      })
      .then(({ body }) => (body.map(label => ({
        id: label.id,
        name: label.name,
        color: label.color
      }))));
    };

    Gitlab.prototype.parseLinkHeaders = function(headers) {
      const re = /<(https\:\/\/gitlab\.com\/api\/v\d\/[^>]+)>; rel="(\w+)"/g;
      let links = {};
      let match;
      while (match = re.exec(headers.link)) {
        links[match[2]] = match[1];
      }
      return links;
    }

    Gitlab.prototype.isLoopDone = function(headers) {
      return headers['x-page'] === headers['x-total-pages'];
    }

    Gitlab.prototype.promiseLoop = function(struct) {
      var that = this;
        if(! struct.isDone) {
          return this.requestStrategy.get({ url: struct.linkHeaders.next, headers: this.requestStrategy.headers})
          .then(({ body, headers }) => {
            const linkHeaders = that.parseLinkHeaders(headers);
            const isDone = that.isLoopDone(headers);
            const iteration = struct.iteration + 1;
            return that.promiseLoop.call(that, {
              linkHeaders, isDone, iteration, headers, body: struct.body.concat(body)
            })
          });
        }
        else {
          console.log(struct.iteration, 'has no more, struct:', struct);
          return Promise.resolve(struct);
        }
    }

    return Gitlab;
  })();


  return function(requestStrategy) {
    const strategies = {
      github: GithubStrategy,
      gitlab: GitlabStrategy,
      bitbucket: BitbucketStrategy
    };
    function factory(k, credentials) {
      return new strategies[k](requestStrategy, credentials);
    }

    return {
      factory
    };
  }

}));

