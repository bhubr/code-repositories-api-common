// Parse Link Headers as given back by GitLab API
// https://gitlab.com/help/api/README.md#pagination-link-header
// https://www.w3.org/wiki/LinkHeader

const re = /<(https\:\/\/gitlab\.com\/api\/v\d\/[^>]+)>; rel="(\w+)"/g;
const headers = {
  link: '<https://gitlab.com/api/v4/projects/lingocentric%2Ftextbeans/issues?id=lingocentric%2Ftextbeans&order_by=created_at&page=2&per_page=20&sort=desc&state=all>; rel="next", <https://gitlab.com/api/v4/projects/lingocentric%2Ftextbeans/issues?id=lingocentric%2Ftextbeans&order_by=created_at&page=1&per_page=20&sort=desc&state=all>; rel="first", <https://gitlab.com/api/v4/projects/lingocentric%2Ftextbeans/issues?id=lingocentric%2Ftextbeans&order_by=created_at&page=4&per_page=20&sort=desc&state=all>; rel="last"'
};
let links = {};

// console.log(re.exec(headers.link));
while (match = re.exec(headers.link)) {
  console.log(match);
  links[match[2]] = match[1];
}
console.log(links);