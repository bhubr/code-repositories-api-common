// const Promise      = require('bluebird');
const path            = require('path');
const chai            = require('chai');
const assert          = chai.assert;
const _               = require('lodash');
const fs              = require('fs');
let RequestStrategy;
try {
  // try require local module (dev)
  RequestStrategy = require('../../code-repositories-api-node/index');
}
catch(e) {
  if(e.name === 'SyntaxError') {
    console.log('# ERR in required module', e);
    process.exit();
  }
  console.log('using published module as fallback', e);
  RequestStrategy = require('code-repositories-api-node');
}
const requestStrategy = new RequestStrategy;
const repoApis        = require('../index')(requestStrategy);
const { accessToken, username, expected } = (require('./accessTokens.json').bitbucket);
const bitbucket          = repoApis.factory('bitbucket', {});
bitbucket.setTokenData({ accessToken, username, scopes: '' });

function id() {
  return (new Date()).getTime().toString(36);
}
describe('CRUD tests', () => {

  describe('issues', () => {

    it('gets issues', () => {
      return bitbucket.getIssuesFor({ name: 'ember-time-tracker' })
      .then(issues => {
        assert.ok(Array.isArray(issues), 'response must be an array');
        const first = _.find(issues, { iid: 1 });
        const { title } = expected.issue;
        assert.equal(first.title, title, 'title should be ' + title);
        return issues;
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
    });

    it('creates issues', () => {
      const title = 'Random Issue ' + id();
      return bitbucket.createIssue({ name: 'test-webhook' }, { title })
      .then(issue => {
        console.log(issue);
        assert.equal(issue.title, title, 'issue title should equal ' + title);
        return issue;
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
    });

  });
});