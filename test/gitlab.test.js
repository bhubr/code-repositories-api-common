// const Promise      = require('bluebird');
const path            = require('path');
const chai            = require('chai');
const assert          = chai.assert;
const _               = require('lodash');
const fs              = require('fs');
// const localReqModule  = path.normalize(__dirname + '/../code-repositories-api-node');
// const hasLocalRequest = fs.existsSync(localReqModule);
let RequestStrategy;
try {
  // try require local module (dev)
  RequestStrategy = require('../../code-repositories-api-node/index');
}
catch(e) {
  if(e.name === 'SyntaxError') {
    console.log('# ERR in required module', e);
    process.exit();
  }
  console.log('using published module as fallback', e);
  RequestStrategy = require('code-repositories-api-node');
}
const requestStrategy = new RequestStrategy;
const repoApis        = require('../index')(requestStrategy);
const { accessToken, username, expected } = (require('./accessTokens.json').gitlab);
const gitlab          = repoApis.factory('gitlab', {});
gitlab.setTokenData({ accessToken, username, scopes: '' });

function id() {
  return (new Date()).getTime().toString(36);
}
describe('CRUD tests', () => {

  describe('issues', () => {

    it('gets issues', () => {
      return gitlab.getIssuesFor({ fullName: 'lingocentric/textbeans' })
      .then(issues => {
        assert.ok(Array.isArray(issues), 'response must be an array');
        const first = _.find(issues, { iid: 1 });
        const { title } = expected.issue;
        assert.equal(first.title, title, 'title should be ' + title);
        return issues;
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
    });


    it('creates issue', () => {
      const title = 'Random Issue ' + id();
      return gitlab.createIssue({ fullName: 'lingocentric/textbeans' }, { title })
      .then(issue => {
        console.log(issue);
        assert.equal(issue.title, title, 'issue title should equal ' + title);
        return issue;
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
    });

  });


  describe('labels', () => {
    let newLabel;
    let tag;

    it('gets labels', () => {
      return gitlab.getLabels({ fullName: 'lingocentric/textbeans' })
      .then(labels => {
        assert.ok(Array.isArray(labels), 'response must be an array');
        const { id, name } = expected.label;
        const first = _.find(labels, { id });
        assert.equal(first.name, name, 'name should be ' + name);
        return labels;
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
    });

    it('creates a label', () => {
      tag = (new Date().getTime()).toString(36);
      const name = 'Random Label ' + tag;
      const color = '#9a9b9c';
      return gitlab.createLabel({ fullName: 'lingocentric/textbeans' }, {
        name, color
      })
      .then(label => {
        newLabel = label;
        assert.equal(label.name, name, 'name should be ' + name);
        assert.equal(label.color, color, 'color should be ' + color);
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
    });

    it('updates a label', () => {
      tag = (new Date().getTime()).toString(36);
      const name = 'Random Label Updated ' + tag;
      const color = '#7a9bbc';
      return gitlab.updateLabel({ fullName: 'lingocentric/textbeans' }, newLabel.name, {
        name, color
      })
      .then(label => {
        newLabel = label;
        assert.equal(label.name, name, 'name should be ' + name);
        assert.equal(label.color, color, 'color should be ' + color);
      })
      .catch(err => {
        // console.log(err);
        throw err;
      })
    });

    it('deletes a label', () => {
      return gitlab.deleteLabel({ fullName: 'lingocentric/textbeans' }, newLabel.name)
      .then(label => {
        // console.log('after delete', label, typeof label);
        // assert.fail(label, 'label should be undefined');
        assert.equal(label, '', 'returned value should be an empty string');
        // assert.equal(label.color, color, 'color should be ' + color);
      })
      .catch(err => {
        // console.log(err);
        throw err;
      })
    });

  });

});