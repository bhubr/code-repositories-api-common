var rp = require('request-promise');
var Promise = require('bluebird');

function Gitlab(cred) {
  this.username = cred.username;
  this.projects = cred.projects;
  this.baseUri = 'https://gitlab.com/api/v4';
  this.options = {
    uri: 'https://gitlab.com/api/v4',
    headers: {
      'PRIVATE-TOKEN': cred.password
    },
    json: true
  };
}

Gitlab.prototype.setPath = function(relativePath) {
  this.options.uri = this.baseUri + relativePath;
};

Gitlab.prototype.fire = function(relativePath) {
  console.log(this.options);
  return rp(this.options);
};

Gitlab.prototype.getProjects = function() {
  // return Promise.map(this.projects, projectName => {
  //   this.setPath('/projects/' + projectName + '/repository/tree');
  //   return this.fire();
  // });
  return this.projects.map(name => ({ name }));
};

Gitlab.prototype.getCommitsFor = function(repoName) {
  this.setPath('/repos/' + this.username + '/' + repoName + '/commits');
  return this.fire()
  .then(commits => { return commits.map(commit => {
    return { sha: commit.sha, message: commit.commit.message };
  }); });
};


Gitlab.prototype.getIssuesFor = function(projectName) {
  this.setPath('/projects/' + projectName + '/issues');
  return this.fire()
  // .then(commits => { return _.map(commits, commit => {
  //   return { sha: commit.sha, message: commit.commit.message };
  // }); });
};


module.exports = Gitlab;